﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;

namespace woolmark_vcsharp
{
    public class Sheep
    {
        private static readonly int SHEEP_IMAGE_HEIGHT;
        private const int DELTA_Y = 3;
        private const int FENCE_BOTTOM_X = 70;
        private const int FENCE_SPACE = 4;
        private const String SHEEP_IMAGE01 = "./res/sheep00.png";
        private const String SHEEP_IMAGE02 = "./res/sheep01.png";
        private static readonly Random r = new Random();
        private static readonly Image[] sheepImages = new Image[2];
        private int x;
        private int y;
        private int jump_x;
        private int canvasWidth;
        private int canvasHeight;
        private Observer observer;
        private bool stretch;
        private SheepState state = SheepState.INACTIVE;

        public static readonly int SHEEP_IMAGE_WIDTH;
        public const int DELTA_X = 5;

        static Sheep()
        {
            sheepImages[0] =Image.FromFile(SHEEP_IMAGE01);
            sheepImages[1] =Image.FromFile(SHEEP_IMAGE02);
            SHEEP_IMAGE_WIDTH = sheepImages[0].Width;
            SHEEP_IMAGE_HEIGHT = sheepImages[0].Height;
        }

        private int calcJumpX(int initY)
        {
            initY -= (canvasHeight - Form1.SKY_HEIGHT);
            return (FENCE_BOTTOM_X - DELTA_Y * initY / FENCE_SPACE);
        }

        public Sheep(Observer observer, int canvasWidth, int canvasHeight)
        {
            this.canvasWidth = canvasWidth;
            this.canvasHeight = canvasHeight;
            this.observer = observer;
        }

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }

        public Image getImage()
        {
            stretch = !stretch;
            if (state == SheepState.JUMP_UP || state == SheepState.JUMP_DOWN || stretch)
            {
                return sheepImages[0];
            }
            else
            {
                return sheepImages[1];
            }
        }

        public void update()
        {
            SheepState currentState = state;
            x -= DELTA_X;
            if (x > jump_x)
            {
                state = SheepState.RUN;
            }
            else if (x > jump_x - SHEEP_IMAGE_WIDTH)
            {
                state = SheepState.JUMP_UP;
                y -= DELTA_Y;
            }
            else if (x > jump_x - 2 * SHEEP_IMAGE_WIDTH)
            {
                state = SheepState.JUMP_DOWN;
                y += DELTA_Y;
            }
            else if (x > -1 * SHEEP_IMAGE_WIDTH)
            {
                state = SheepState.RUN;
            }
            else
            {
                state = SheepState.INACTIVE;
            }
            if (currentState != state)
            {
                observer.notifyObserver(this, state);
            }
        }

        public void resurrection()
        {
            x = canvasWidth + SHEEP_IMAGE_WIDTH;
            y = Form1.SKY_HEIGHT + r.Next(canvasHeight - Form1.SKY_HEIGHT - SHEEP_IMAGE_HEIGHT);
            jump_x = calcJumpX(y);
        }

        public enum SheepState
        {
            INACTIVE, RUN, JUMP_UP, JUMP_DOWN
        }
    }
}
