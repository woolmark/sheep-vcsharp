﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace woolmark_vcsharp
{
    public interface Observer
    {
        void notifyObserver(Sheep sheep, Sheep.SheepState state);
    }
}
