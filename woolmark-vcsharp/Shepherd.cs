﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace woolmark_vcsharp
{
    class Shepherd : Observer
    {
        private static readonly List<Sheep> activeSheep = new List<Sheep>();
        private static readonly List<Sheep> inactiveSheep = new List<Sheep>();
        private int sheepNumber = 0;

        public Shepherd(int width, int height)
        {
            int sheepMax = (width + 2 * Sheep.SHEEP_IMAGE_WIDTH + (Sheep.DELTA_X - 1)) / Sheep.DELTA_X + Form1.INITIAL_SHEEP_NUM;
            for (int i = 0; i < sheepMax; i++)
            {
                inactiveSheep.Add(new Sheep(this, width, height));
            }
        }

        public void notifyObserver(Sheep sheep, Sheep.SheepState state)
        {
            switch (state)
            {
                case Sheep.SheepState.JUMP_DOWN:
                    sheepNumber++;
                    break;
                case Sheep.SheepState.INACTIVE:
                    lock (activeSheep)
                    {
                        activeSheep.Remove(sheep);
                        inactiveSheep.Add(sheep);
                        if (activeSheep.Count == 0)
                        {
                            addSheep();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        public int getSheepNumber()
        {
            return sheepNumber;
        }

        public void addSheep()
        {
            Sheep s = inactiveSheep[0];
            inactiveSheep.RemoveAt(0);
            s.resurrection();
            lock (activeSheep)
            {
                activeSheep.Add(s);
            }
        }

        public Sheep[] getSheep()
        {
            return activeSheep.ToArray();
        }

        public void run()
        {
            while (true) {
                Sheep[] sheep;
                lock (activeSheep)
                {
                    sheep = activeSheep.ToArray();
                    for (int i = 0; i < sheep.Length; i++)
                    {
                        sheep[i].update();
                    }
                }
                Thread.Sleep(Form1.SPEED);
            }
        }
    }
}
