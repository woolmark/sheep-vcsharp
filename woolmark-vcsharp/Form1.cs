﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace woolmark_vcsharp
{
    public partial class Form1 : Form
    {
        private static readonly Color SKY_COLOR = Color.FromArgb(150, 150, 255);
        private static readonly Color GROUND_COLOR = Color.FromArgb(100, 255, 100);
        private static readonly Color MESSAGE_COLOR = Color.Black;
        private const String MESSAGE_FORMAT = "羊が{0}匹";
        private const String FENCE_IMAGE = "./res/fence.png";
        private const int MESSAGE_X = 5;
        private const int MESSAGE_Y = 15;
        private const int WINDOWS_MERGIN = 24;
        private const int FONT_SIZE = 8;
        private int width;
        private int height;
        private int fence_x;
        private int fence_y;
        private static Image fence;
        private bool isMousePressed;
        private Shepherd shepherd;
        private Bitmap canvas;
        private SolidBrush grandBrush;
        private SolidBrush skyBrush;
        private SolidBrush messageBrush;
        private Point paintPoint;
        private Font fnt;

        public const int INITIAL_SHEEP_NUM = 1;
        public const int SKY_HEIGHT = 50;
        public const int SPEED = 60;

        static Form1()
        {
            fence = Image.FromFile(FENCE_IMAGE);
        }

        public Form1()
        {
            InitializeComponent();
            width = Width;
            height = Height - WINDOWS_MERGIN;
            fence_x = (width - fence.Width) / 2;
            fence_y = height - fence.Height;
            canvas = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            grandBrush = new SolidBrush(GROUND_COLOR);
            skyBrush = new SolidBrush(SKY_COLOR);
            messageBrush = new SolidBrush(MESSAGE_COLOR);
            paintPoint = new Point(0, 0);
            fnt = new Font("MS UI Gothic", FONT_SIZE);

            shepherd = new Shepherd(width, height);
            for (int i = 0; i < INITIAL_SHEEP_NUM; i++)
            {
                shepherd.addSheep();
            }
            Thread shepherdThread = new Thread(shepherd.run);
            Thread drawingThread = new Thread(this.run);
            shepherdThread.Start();
            drawingThread.Start();
        }

        public void run()
        {
            while(true) {
                if (isMousePressed)
                {
                    shepherd.addSheep();
                }
                Thread.Sleep(SPEED);
            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void setPaintPoint(int x, int y)
        {
            paintPoint.X = x;
            paintPoint.Y = y;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = Graphics.FromImage(canvas);

            g.FillRectangle(grandBrush, 0, 0, width, height);
            g.FillRectangle(skyBrush, 0, 0, width, SKY_HEIGHT);

            setPaintPoint(fence_x, fence_y);
            g.DrawImage(fence, paintPoint);

            Sheep[] sheeps = shepherd.getSheep();
            if (sheeps != null) {
                for (int i = 0; i < sheeps.Length; i++)
                {
                    setPaintPoint(sheeps[i].getX(), sheeps[i].getY());
                    g.DrawImage(sheeps[i].getImage(), paintPoint);
                }
            }

            setPaintPoint(MESSAGE_X, MESSAGE_Y);
            g.DrawString(String.Format(MESSAGE_FORMAT, shepherd.getSheepNumber()), fnt, messageBrush, paintPoint);

            g.Dispose();

            //PictureBox1に表示する
            pictureBox1.Image = canvas;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMousePressed = true;
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMousePressed = false;
            }
        }
    }
}
